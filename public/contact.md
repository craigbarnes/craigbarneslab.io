Contact Information for @craigbarnes
====================================

For inquiries about any of my open source [projects], I can be contacted
at the email address printed by:

    echo ofmisnmfbqgzdfchcbamilkoca | tr zka-hm-t @.m-ta-h

My PGP public key is available from [here][pubkey] or various [keyservers][]
(fingerprint: `A3FB 922E 1587 0DA2 50D9  DCE1 FBCC 7A6B 0330 BEB4`).


[projects]: https://gitlab.com/users/craigbarnes/projects
[pubkey]: https://craigbarnes.gitlab.io/pubkey/0330BEB4.asc
[keyservers]: https://keys.openpgp.org/
