PANDOC = pandoc
DIST_NAMES = dte lua-discount lua-gumbo lua-sass lua-createtable showdown
TARBALLS = $(wildcard $(foreach D, $(DIST_NAMES), public/dist/$D/$D-*.tar.gz))
GPGSIGS = $(addsuffix .sig, $(TARBALLS))
SCREENSHOTS = $(wildcard public/screenshot/*.png)
PUBKEYS = $(addprefix public/pubkey/0330BEB4, .gpg .asc)
PUBKEY = $(firstword $(PUBKEYS))

SHA256SUMS = \
    $(foreach D, $(DIST_NAMES), public/dist/$D/$D-sha256sums.txt) \
    $(foreach D, pubkey screenshot, public/$D/sha256sums.txt)

generate: $(GPGSIGS) $(SHA256SUMS) docs
docs: public/contact.html
check: check-signatures check-digests

update-pubkeys:
	gpg --yes --output public/pubkey/0330BEB4.asc --armor --export 0330BEB4
	gpg --yes --output public/pubkey/0330BEB4.gpg --export 0330BEB4
	$(MAKE) --no-print-directory public/pubkey/sha256sums.txt

check-signatures:
	@for file in $(GPGSIGS); do \
	  gpg --no-default-keyring --keyring '$(PUBKEY)' --verify "$$file" && \
	  echo; \
	done

check-digests:
	@: $(foreach F, $(SHA256SUMS), \
	  && printf '\n$(F):\n' \
	  && cd '$(CURDIR)/$(dir $(F))' \
	  && sha256sum -c $(notdir $(F)) )

public/contact.html: public/contact.md template.html
	$(PANDOC) -s -f gfm -t html5 --template=template.html -Mtitle=_ -o $@ $<

$(GPGSIGS): %.tar.gz.sig: %.tar.gz
	test -f '$@' || gpg -u 0x0330BEB4 -b -o '$@' '$<'

$(SHA256SUMS):
	cd $(@D) && sha256sum $(^F) | sort -V -r -k2 > $(@F)

$(foreach F, $(SHA256SUMS), $(eval \
  $(F): $(filter $(dir $(F))%, $(TARBALLS) $(GPGSIGS)) \
))

public/pubkey/sha256sums.txt: $(PUBKEYS)
public/screenshot/sha256sums.txt: $(SCREENSHOTS)


.PHONY: generate docs check check-signatures check-digests update-pubkeys
.DELETE_ON_ERROR:
